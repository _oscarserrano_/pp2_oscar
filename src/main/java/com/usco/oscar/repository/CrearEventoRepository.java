package com.usco.oscar.repository;

import com.usco.oscar.domain.CrearEvento;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CrearEvento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CrearEventoRepository extends JpaRepository<CrearEvento, Long>, JpaSpecificationExecutor<CrearEvento> {
}

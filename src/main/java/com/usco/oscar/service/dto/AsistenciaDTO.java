package com.usco.oscar.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.usco.oscar.domain.Asistencia} entity.
 */
public class AsistenciaDTO implements Serializable {
    
    private Long id;

    private Long metodoPago;

    private Long usuaroId;

    private Long eventoId;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(Long metodoPago) {
        this.metodoPago = metodoPago;
    }

    public Long getUsuaroId() {
        return usuaroId;
    }

    public void setUsuaroId(Long usuaroId) {
        this.usuaroId = usuaroId;
    }

    public Long getEventoId() {
        return eventoId;
    }

    public void setEventoId(Long eventoId) {
        this.eventoId = eventoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AsistenciaDTO)) {
            return false;
        }

        return id != null && id.equals(((AsistenciaDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AsistenciaDTO{" +
            "id=" + getId() +
            ", metodoPago=" + getMetodoPago() +
            ", usuaroId=" + getUsuaroId() +
            ", eventoId=" + getEventoId() +
            "}";
    }
}

package com.usco.oscar.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.usco.oscar.domain.CrearEvento} entity. This class is used
 * in {@link com.usco.oscar.web.rest.CrearEventoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /crear-eventos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CrearEventoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nombreEvento;

    private StringFilter informacion;

    private StringFilter lugar;

    private InstantFilter fechaHora;

    private LongFilter usuarioId;

    private BooleanFilter tipoEvento;

    private LongFilter cantidadBoletas;

    private StringFilter categoriaEvento;

    private FloatFilter precioBoleta;

    public CrearEventoCriteria() {
    }

    public CrearEventoCriteria(CrearEventoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nombreEvento = other.nombreEvento == null ? null : other.nombreEvento.copy();
        this.informacion = other.informacion == null ? null : other.informacion.copy();
        this.lugar = other.lugar == null ? null : other.lugar.copy();
        this.fechaHora = other.fechaHora == null ? null : other.fechaHora.copy();
        this.usuarioId = other.usuarioId == null ? null : other.usuarioId.copy();
        this.tipoEvento = other.tipoEvento == null ? null : other.tipoEvento.copy();
        this.cantidadBoletas = other.cantidadBoletas == null ? null : other.cantidadBoletas.copy();
        this.categoriaEvento = other.categoriaEvento == null ? null : other.categoriaEvento.copy();
        this.precioBoleta = other.precioBoleta == null ? null : other.precioBoleta.copy();
    }

    @Override
    public CrearEventoCriteria copy() {
        return new CrearEventoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(StringFilter nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public StringFilter getInformacion() {
        return informacion;
    }

    public void setInformacion(StringFilter informacion) {
        this.informacion = informacion;
    }

    public StringFilter getLugar() {
        return lugar;
    }

    public void setLugar(StringFilter lugar) {
        this.lugar = lugar;
    }

    public InstantFilter getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(InstantFilter fechaHora) {
        this.fechaHora = fechaHora;
    }

    public LongFilter getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(LongFilter usuarioId) {
        this.usuarioId = usuarioId;
    }

    public BooleanFilter getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(BooleanFilter tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public LongFilter getCantidadBoletas() {
        return cantidadBoletas;
    }

    public void setCantidadBoletas(LongFilter cantidadBoletas) {
        this.cantidadBoletas = cantidadBoletas;
    }

    public StringFilter getCategoriaEvento() {
        return categoriaEvento;
    }

    public void setCategoriaEvento(StringFilter categoriaEvento) {
        this.categoriaEvento = categoriaEvento;
    }

    public FloatFilter getPrecioBoleta() {
        return precioBoleta;
    }

    public void setPrecioBoleta(FloatFilter precioBoleta) {
        this.precioBoleta = precioBoleta;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CrearEventoCriteria that = (CrearEventoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nombreEvento, that.nombreEvento) &&
            Objects.equals(informacion, that.informacion) &&
            Objects.equals(lugar, that.lugar) &&
            Objects.equals(fechaHora, that.fechaHora) &&
            Objects.equals(usuarioId, that.usuarioId) &&
            Objects.equals(tipoEvento, that.tipoEvento) &&
            Objects.equals(cantidadBoletas, that.cantidadBoletas) &&
            Objects.equals(categoriaEvento, that.categoriaEvento) &&
            Objects.equals(precioBoleta, that.precioBoleta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nombreEvento,
        informacion,
        lugar,
        fechaHora,
        usuarioId,
        tipoEvento,
        cantidadBoletas,
        categoriaEvento,
        precioBoleta
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CrearEventoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nombreEvento != null ? "nombreEvento=" + nombreEvento + ", " : "") +
                (informacion != null ? "informacion=" + informacion + ", " : "") +
                (lugar != null ? "lugar=" + lugar + ", " : "") +
                (fechaHora != null ? "fechaHora=" + fechaHora + ", " : "") +
                (usuarioId != null ? "usuarioId=" + usuarioId + ", " : "") +
                (tipoEvento != null ? "tipoEvento=" + tipoEvento + ", " : "") +
                (cantidadBoletas != null ? "cantidadBoletas=" + cantidadBoletas + ", " : "") +
                (categoriaEvento != null ? "categoriaEvento=" + categoriaEvento + ", " : "") +
                (precioBoleta != null ? "precioBoleta=" + precioBoleta + ", " : "") +
            "}";
    }

}

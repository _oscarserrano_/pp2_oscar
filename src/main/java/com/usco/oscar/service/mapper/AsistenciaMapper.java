package com.usco.oscar.service.mapper;


import com.usco.oscar.domain.*;
import com.usco.oscar.service.dto.AsistenciaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Asistencia} and its DTO {@link AsistenciaDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AsistenciaMapper extends EntityMapper<AsistenciaDTO, Asistencia> {



    default Asistencia fromId(Long id) {
        if (id == null) {
            return null;
        }
        Asistencia asistencia = new Asistencia();
        asistencia.setId(id);
        return asistencia;
    }
}

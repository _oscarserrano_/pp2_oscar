package com.usco.oscar.service.mapper;


import com.usco.oscar.domain.*;
import com.usco.oscar.service.dto.CrearEventoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CrearEvento} and its DTO {@link CrearEventoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CrearEventoMapper extends EntityMapper<CrearEventoDTO, CrearEvento> {



    default CrearEvento fromId(Long id) {
        if (id == null) {
            return null;
        }
        CrearEvento crearEvento = new CrearEvento();
        crearEvento.setId(id);
        return crearEvento;
    }
}

/**
 * View Models used by Spring MVC REST controllers.
 */
package com.usco.oscar.web.rest.vm;

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { Pp2OscarSharedModule } from 'app/shared/shared.module';
import { Pp2OscarCoreModule } from 'app/core/core.module';
import { Pp2OscarAppRoutingModule } from './app-routing.module';
import { Pp2OscarHomeModule } from './home/home.module';
import { Pp2OscarEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    Pp2OscarSharedModule,
    Pp2OscarCoreModule,
    Pp2OscarHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    Pp2OscarEntityModule,
    Pp2OscarAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class Pp2OscarAppModule {}

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAsistencia } from 'app/shared/model/asistencia.model';
import { AsistenciaService } from './asistencia.service';

@Component({
  templateUrl: './asistencia-delete-dialog.component.html',
})
export class AsistenciaDeleteDialogComponent {
  asistencia?: IAsistencia;

  constructor(
    protected asistenciaService: AsistenciaService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.asistenciaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('asistenciaListModification');
      this.activeModal.close();
    });
  }
}

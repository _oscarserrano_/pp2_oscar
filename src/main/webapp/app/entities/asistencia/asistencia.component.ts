import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAsistencia } from 'app/shared/model/asistencia.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { AsistenciaService } from './asistencia.service';
import { AsistenciaDeleteDialogComponent } from './asistencia-delete-dialog.component';

@Component({
  selector: 'jhi-asistencia',
  templateUrl: './asistencia.component.html',
})
export class AsistenciaComponent implements OnInit, OnDestroy {
  asistencias: IAsistencia[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected asistenciaService: AsistenciaService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.asistencias = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.asistenciaService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IAsistencia[]>) => this.paginateAsistencias(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.asistencias = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAsistencias();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAsistencia): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAsistencias(): void {
    this.eventSubscriber = this.eventManager.subscribe('asistenciaListModification', () => this.reset());
  }

  delete(asistencia: IAsistencia): void {
    const modalRef = this.modalService.open(AsistenciaDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.asistencia = asistencia;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateAsistencias(data: IAsistencia[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.asistencias.push(data[i]);
      }
    }
  }
}

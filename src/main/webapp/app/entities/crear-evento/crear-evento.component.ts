import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICrearEvento } from 'app/shared/model/crear-evento.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { CrearEventoService } from './crear-evento.service';
import { CrearEventoDeleteDialogComponent } from './crear-evento-delete-dialog.component';

@Component({
  selector: 'jhi-crear-evento',
  templateUrl: './crear-evento.component.html',
})
export class CrearEventoComponent implements OnInit, OnDestroy {
  crearEventos: ICrearEvento[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected crearEventoService: CrearEventoService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.crearEventos = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.crearEventoService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ICrearEvento[]>) => this.paginateCrearEventos(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.crearEventos = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCrearEventos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICrearEvento): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCrearEventos(): void {
    this.eventSubscriber = this.eventManager.subscribe('crearEventoListModification', () => this.reset());
  }

  delete(crearEvento: ICrearEvento): void {
    const modalRef = this.modalService.open(CrearEventoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.crearEvento = crearEvento;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateCrearEventos(data: ICrearEvento[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.crearEventos.push(data[i]);
      }
    }
  }
}

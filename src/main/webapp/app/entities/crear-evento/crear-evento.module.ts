import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Pp2OscarSharedModule } from 'app/shared/shared.module';
import { CrearEventoComponent } from './crear-evento.component';
import { CrearEventoDetailComponent } from './crear-evento-detail.component';
import { CrearEventoUpdateComponent } from './crear-evento-update.component';
import { CrearEventoDeleteDialogComponent } from './crear-evento-delete-dialog.component';
import { crearEventoRoute } from './crear-evento.route';

@NgModule({
  imports: [Pp2OscarSharedModule, RouterModule.forChild(crearEventoRoute)],
  declarations: [CrearEventoComponent, CrearEventoDetailComponent, CrearEventoUpdateComponent, CrearEventoDeleteDialogComponent],
  entryComponents: [CrearEventoDeleteDialogComponent],
})
export class Pp2OscarCrearEventoModule {}

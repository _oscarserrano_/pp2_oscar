import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'usuario',
        loadChildren: () => import('./usuario/usuario.module').then(m => m.Pp2OscarUsuarioModule),
      },
      {
        path: 'crear-evento',
        loadChildren: () => import('./crear-evento/crear-evento.module').then(m => m.Pp2OscarCrearEventoModule),
      },
      {
        path: 'asistencia',
        loadChildren: () => import('./asistencia/asistencia.module').then(m => m.Pp2OscarAsistenciaModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class Pp2OscarEntityModule {}

import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-home-crear-evento',
  templateUrl: './home-crear-evento.component.html',
  styleUrls: ['./home-crear-evento.component.scss']
})
export class HomeCrearEventoComponent implements OnInit {

  dataEvento = this.fb.group({

  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

}

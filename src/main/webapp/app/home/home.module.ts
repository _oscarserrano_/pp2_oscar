import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Pp2OscarSharedModule } from 'app/shared/shared.module';
import { MHHOME_ROUTES } from './home.route';
import { HomeComponent } from './home.component';
import { HomeCrearEventoComponent } from './home-crear-evento/home-crear-evento.component';

@NgModule({
  imports: [Pp2OscarSharedModule, RouterModule.forChild([MHHOME_ROUTES])],
  declarations: [HomeComponent, HomeCrearEventoComponent],
})
export class Pp2OscarHomeModule {}

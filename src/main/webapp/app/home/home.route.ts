import { HomeCrearEventoComponent } from './home-crear-evento/home-crear-evento.component';
import { Component } from '@angular/core';
import { Route } from '@angular/router';

import { HomeComponent } from './home.component';

export const HOME_ROUTE: Route = {
  path: '',
  component: HomeComponent,
  data: {
    authorities: [],
    pageTitle: 'home.title',
  },
};

export const HCREAR_EVENTO_ROUTE: Route = {
  path:'home-crear-evento',
  component: HomeCrearEventoComponent,
  data: {
    authorities: [],
    pageTitle: 'home.title',
  },
}

const MHOME_ROUTES = [HOME_ROUTE, HCREAR_EVENTO_ROUTE];

export const MHHOME_ROUTES: Route = {
  path: '',
  children: MHOME_ROUTES
}

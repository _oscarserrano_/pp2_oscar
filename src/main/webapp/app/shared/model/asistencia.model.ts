export interface IAsistencia {
  id?: number;
  metodoPago?: number;
  usuaroId?: number;
  eventoId?: number;
}

export class Asistencia implements IAsistencia {
  constructor(public id?: number, public metodoPago?: number, public usuaroId?: number, public eventoId?: number) {}
}

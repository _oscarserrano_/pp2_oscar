import { Moment } from 'moment';

export interface ICrearEvento {
  id?: number;
  nombreEvento?: string;
  informacion?: string;
  lugar?: string;
  fechaHora?: Moment;
  usuarioId?: number;
  tipoEvento?: boolean;
  cantidadBoletas?: number;
  categoriaEvento?: string;
  precioBoleta?: number;
}

export class CrearEvento implements ICrearEvento {
  constructor(
    public id?: number,
    public nombreEvento?: string,
    public informacion?: string,
    public lugar?: string,
    public fechaHora?: Moment,
    public usuarioId?: number,
    public tipoEvento?: boolean,
    public cantidadBoletas?: number,
    public categoriaEvento?: string,
    public precioBoleta?: number
  ) {
    this.tipoEvento = this.tipoEvento || false;
  }
}

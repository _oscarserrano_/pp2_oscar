export interface IUsuario {
  id?: number;
  loginUsuario?: string;
  nombreUsuario?: string;
  foto?: string;
  userId?: number;
  tipoDocumento?: number;
  numeroDocumento?: number;
  telefono?: number;
}

export class Usuario implements IUsuario {
  constructor(
    public id?: number,
    public loginUsuario?: string,
    public nombreUsuario?: string,
    public foto?: string,
    public userId?: number,
    public tipoDocumento?: number,
    public numeroDocumento?: number,
    public telefono?: number
  ) {}
}

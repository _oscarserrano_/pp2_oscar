package com.usco.oscar.web.rest;

import com.usco.oscar.Pp2OscarApp;
import com.usco.oscar.domain.Asistencia;
import com.usco.oscar.repository.AsistenciaRepository;
import com.usco.oscar.service.AsistenciaService;
import com.usco.oscar.service.dto.AsistenciaDTO;
import com.usco.oscar.service.mapper.AsistenciaMapper;
import com.usco.oscar.service.dto.AsistenciaCriteria;
import com.usco.oscar.service.AsistenciaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AsistenciaResource} REST controller.
 */
@SpringBootTest(classes = Pp2OscarApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AsistenciaResourceIT {

    private static final Long DEFAULT_METODO_PAGO = 1L;
    private static final Long UPDATED_METODO_PAGO = 2L;
    private static final Long SMALLER_METODO_PAGO = 1L - 1L;

    private static final Long DEFAULT_USUARO_ID = 1L;
    private static final Long UPDATED_USUARO_ID = 2L;
    private static final Long SMALLER_USUARO_ID = 1L - 1L;

    private static final Long DEFAULT_EVENTO_ID = 1L;
    private static final Long UPDATED_EVENTO_ID = 2L;
    private static final Long SMALLER_EVENTO_ID = 1L - 1L;

    @Autowired
    private AsistenciaRepository asistenciaRepository;

    @Autowired
    private AsistenciaMapper asistenciaMapper;

    @Autowired
    private AsistenciaService asistenciaService;

    @Autowired
    private AsistenciaQueryService asistenciaQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAsistenciaMockMvc;

    private Asistencia asistencia;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asistencia createEntity(EntityManager em) {
        Asistencia asistencia = new Asistencia()
            .metodoPago(DEFAULT_METODO_PAGO)
            .usuaroId(DEFAULT_USUARO_ID)
            .eventoId(DEFAULT_EVENTO_ID);
        return asistencia;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asistencia createUpdatedEntity(EntityManager em) {
        Asistencia asistencia = new Asistencia()
            .metodoPago(UPDATED_METODO_PAGO)
            .usuaroId(UPDATED_USUARO_ID)
            .eventoId(UPDATED_EVENTO_ID);
        return asistencia;
    }

    @BeforeEach
    public void initTest() {
        asistencia = createEntity(em);
    }

    @Test
    @Transactional
    public void createAsistencia() throws Exception {
        int databaseSizeBeforeCreate = asistenciaRepository.findAll().size();
        // Create the Asistencia
        AsistenciaDTO asistenciaDTO = asistenciaMapper.toDto(asistencia);
        restAsistenciaMockMvc.perform(post("/api/asistencias")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asistenciaDTO)))
            .andExpect(status().isCreated());

        // Validate the Asistencia in the database
        List<Asistencia> asistenciaList = asistenciaRepository.findAll();
        assertThat(asistenciaList).hasSize(databaseSizeBeforeCreate + 1);
        Asistencia testAsistencia = asistenciaList.get(asistenciaList.size() - 1);
        assertThat(testAsistencia.getMetodoPago()).isEqualTo(DEFAULT_METODO_PAGO);
        assertThat(testAsistencia.getUsuaroId()).isEqualTo(DEFAULT_USUARO_ID);
        assertThat(testAsistencia.getEventoId()).isEqualTo(DEFAULT_EVENTO_ID);
    }

    @Test
    @Transactional
    public void createAsistenciaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = asistenciaRepository.findAll().size();

        // Create the Asistencia with an existing ID
        asistencia.setId(1L);
        AsistenciaDTO asistenciaDTO = asistenciaMapper.toDto(asistencia);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAsistenciaMockMvc.perform(post("/api/asistencias")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asistenciaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Asistencia in the database
        List<Asistencia> asistenciaList = asistenciaRepository.findAll();
        assertThat(asistenciaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAsistencias() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList
        restAsistenciaMockMvc.perform(get("/api/asistencias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(asistencia.getId().intValue())))
            .andExpect(jsonPath("$.[*].metodoPago").value(hasItem(DEFAULT_METODO_PAGO.intValue())))
            .andExpect(jsonPath("$.[*].usuaroId").value(hasItem(DEFAULT_USUARO_ID.intValue())))
            .andExpect(jsonPath("$.[*].eventoId").value(hasItem(DEFAULT_EVENTO_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getAsistencia() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get the asistencia
        restAsistenciaMockMvc.perform(get("/api/asistencias/{id}", asistencia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(asistencia.getId().intValue()))
            .andExpect(jsonPath("$.metodoPago").value(DEFAULT_METODO_PAGO.intValue()))
            .andExpect(jsonPath("$.usuaroId").value(DEFAULT_USUARO_ID.intValue()))
            .andExpect(jsonPath("$.eventoId").value(DEFAULT_EVENTO_ID.intValue()));
    }


    @Test
    @Transactional
    public void getAsistenciasByIdFiltering() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        Long id = asistencia.getId();

        defaultAsistenciaShouldBeFound("id.equals=" + id);
        defaultAsistenciaShouldNotBeFound("id.notEquals=" + id);

        defaultAsistenciaShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAsistenciaShouldNotBeFound("id.greaterThan=" + id);

        defaultAsistenciaShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAsistenciaShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago equals to DEFAULT_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.equals=" + DEFAULT_METODO_PAGO);

        // Get all the asistenciaList where metodoPago equals to UPDATED_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.equals=" + UPDATED_METODO_PAGO);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago not equals to DEFAULT_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.notEquals=" + DEFAULT_METODO_PAGO);

        // Get all the asistenciaList where metodoPago not equals to UPDATED_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.notEquals=" + UPDATED_METODO_PAGO);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsInShouldWork() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago in DEFAULT_METODO_PAGO or UPDATED_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.in=" + DEFAULT_METODO_PAGO + "," + UPDATED_METODO_PAGO);

        // Get all the asistenciaList where metodoPago equals to UPDATED_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.in=" + UPDATED_METODO_PAGO);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsNullOrNotNull() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago is not null
        defaultAsistenciaShouldBeFound("metodoPago.specified=true");

        // Get all the asistenciaList where metodoPago is null
        defaultAsistenciaShouldNotBeFound("metodoPago.specified=false");
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago is greater than or equal to DEFAULT_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.greaterThanOrEqual=" + DEFAULT_METODO_PAGO);

        // Get all the asistenciaList where metodoPago is greater than or equal to UPDATED_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.greaterThanOrEqual=" + UPDATED_METODO_PAGO);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago is less than or equal to DEFAULT_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.lessThanOrEqual=" + DEFAULT_METODO_PAGO);

        // Get all the asistenciaList where metodoPago is less than or equal to SMALLER_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.lessThanOrEqual=" + SMALLER_METODO_PAGO);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsLessThanSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago is less than DEFAULT_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.lessThan=" + DEFAULT_METODO_PAGO);

        // Get all the asistenciaList where metodoPago is less than UPDATED_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.lessThan=" + UPDATED_METODO_PAGO);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByMetodoPagoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where metodoPago is greater than DEFAULT_METODO_PAGO
        defaultAsistenciaShouldNotBeFound("metodoPago.greaterThan=" + DEFAULT_METODO_PAGO);

        // Get all the asistenciaList where metodoPago is greater than SMALLER_METODO_PAGO
        defaultAsistenciaShouldBeFound("metodoPago.greaterThan=" + SMALLER_METODO_PAGO);
    }


    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId equals to DEFAULT_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.equals=" + DEFAULT_USUARO_ID);

        // Get all the asistenciaList where usuaroId equals to UPDATED_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.equals=" + UPDATED_USUARO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId not equals to DEFAULT_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.notEquals=" + DEFAULT_USUARO_ID);

        // Get all the asistenciaList where usuaroId not equals to UPDATED_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.notEquals=" + UPDATED_USUARO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsInShouldWork() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId in DEFAULT_USUARO_ID or UPDATED_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.in=" + DEFAULT_USUARO_ID + "," + UPDATED_USUARO_ID);

        // Get all the asistenciaList where usuaroId equals to UPDATED_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.in=" + UPDATED_USUARO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId is not null
        defaultAsistenciaShouldBeFound("usuaroId.specified=true");

        // Get all the asistenciaList where usuaroId is null
        defaultAsistenciaShouldNotBeFound("usuaroId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId is greater than or equal to DEFAULT_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.greaterThanOrEqual=" + DEFAULT_USUARO_ID);

        // Get all the asistenciaList where usuaroId is greater than or equal to UPDATED_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.greaterThanOrEqual=" + UPDATED_USUARO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId is less than or equal to DEFAULT_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.lessThanOrEqual=" + DEFAULT_USUARO_ID);

        // Get all the asistenciaList where usuaroId is less than or equal to SMALLER_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.lessThanOrEqual=" + SMALLER_USUARO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsLessThanSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId is less than DEFAULT_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.lessThan=" + DEFAULT_USUARO_ID);

        // Get all the asistenciaList where usuaroId is less than UPDATED_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.lessThan=" + UPDATED_USUARO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByUsuaroIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where usuaroId is greater than DEFAULT_USUARO_ID
        defaultAsistenciaShouldNotBeFound("usuaroId.greaterThan=" + DEFAULT_USUARO_ID);

        // Get all the asistenciaList where usuaroId is greater than SMALLER_USUARO_ID
        defaultAsistenciaShouldBeFound("usuaroId.greaterThan=" + SMALLER_USUARO_ID);
    }


    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId equals to DEFAULT_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.equals=" + DEFAULT_EVENTO_ID);

        // Get all the asistenciaList where eventoId equals to UPDATED_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.equals=" + UPDATED_EVENTO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId not equals to DEFAULT_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.notEquals=" + DEFAULT_EVENTO_ID);

        // Get all the asistenciaList where eventoId not equals to UPDATED_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.notEquals=" + UPDATED_EVENTO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsInShouldWork() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId in DEFAULT_EVENTO_ID or UPDATED_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.in=" + DEFAULT_EVENTO_ID + "," + UPDATED_EVENTO_ID);

        // Get all the asistenciaList where eventoId equals to UPDATED_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.in=" + UPDATED_EVENTO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId is not null
        defaultAsistenciaShouldBeFound("eventoId.specified=true");

        // Get all the asistenciaList where eventoId is null
        defaultAsistenciaShouldNotBeFound("eventoId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId is greater than or equal to DEFAULT_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.greaterThanOrEqual=" + DEFAULT_EVENTO_ID);

        // Get all the asistenciaList where eventoId is greater than or equal to UPDATED_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.greaterThanOrEqual=" + UPDATED_EVENTO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId is less than or equal to DEFAULT_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.lessThanOrEqual=" + DEFAULT_EVENTO_ID);

        // Get all the asistenciaList where eventoId is less than or equal to SMALLER_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.lessThanOrEqual=" + SMALLER_EVENTO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsLessThanSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId is less than DEFAULT_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.lessThan=" + DEFAULT_EVENTO_ID);

        // Get all the asistenciaList where eventoId is less than UPDATED_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.lessThan=" + UPDATED_EVENTO_ID);
    }

    @Test
    @Transactional
    public void getAllAsistenciasByEventoIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        // Get all the asistenciaList where eventoId is greater than DEFAULT_EVENTO_ID
        defaultAsistenciaShouldNotBeFound("eventoId.greaterThan=" + DEFAULT_EVENTO_ID);

        // Get all the asistenciaList where eventoId is greater than SMALLER_EVENTO_ID
        defaultAsistenciaShouldBeFound("eventoId.greaterThan=" + SMALLER_EVENTO_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAsistenciaShouldBeFound(String filter) throws Exception {
        restAsistenciaMockMvc.perform(get("/api/asistencias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(asistencia.getId().intValue())))
            .andExpect(jsonPath("$.[*].metodoPago").value(hasItem(DEFAULT_METODO_PAGO.intValue())))
            .andExpect(jsonPath("$.[*].usuaroId").value(hasItem(DEFAULT_USUARO_ID.intValue())))
            .andExpect(jsonPath("$.[*].eventoId").value(hasItem(DEFAULT_EVENTO_ID.intValue())));

        // Check, that the count call also returns 1
        restAsistenciaMockMvc.perform(get("/api/asistencias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAsistenciaShouldNotBeFound(String filter) throws Exception {
        restAsistenciaMockMvc.perform(get("/api/asistencias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAsistenciaMockMvc.perform(get("/api/asistencias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAsistencia() throws Exception {
        // Get the asistencia
        restAsistenciaMockMvc.perform(get("/api/asistencias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAsistencia() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        int databaseSizeBeforeUpdate = asistenciaRepository.findAll().size();

        // Update the asistencia
        Asistencia updatedAsistencia = asistenciaRepository.findById(asistencia.getId()).get();
        // Disconnect from session so that the updates on updatedAsistencia are not directly saved in db
        em.detach(updatedAsistencia);
        updatedAsistencia
            .metodoPago(UPDATED_METODO_PAGO)
            .usuaroId(UPDATED_USUARO_ID)
            .eventoId(UPDATED_EVENTO_ID);
        AsistenciaDTO asistenciaDTO = asistenciaMapper.toDto(updatedAsistencia);

        restAsistenciaMockMvc.perform(put("/api/asistencias")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asistenciaDTO)))
            .andExpect(status().isOk());

        // Validate the Asistencia in the database
        List<Asistencia> asistenciaList = asistenciaRepository.findAll();
        assertThat(asistenciaList).hasSize(databaseSizeBeforeUpdate);
        Asistencia testAsistencia = asistenciaList.get(asistenciaList.size() - 1);
        assertThat(testAsistencia.getMetodoPago()).isEqualTo(UPDATED_METODO_PAGO);
        assertThat(testAsistencia.getUsuaroId()).isEqualTo(UPDATED_USUARO_ID);
        assertThat(testAsistencia.getEventoId()).isEqualTo(UPDATED_EVENTO_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingAsistencia() throws Exception {
        int databaseSizeBeforeUpdate = asistenciaRepository.findAll().size();

        // Create the Asistencia
        AsistenciaDTO asistenciaDTO = asistenciaMapper.toDto(asistencia);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAsistenciaMockMvc.perform(put("/api/asistencias")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asistenciaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Asistencia in the database
        List<Asistencia> asistenciaList = asistenciaRepository.findAll();
        assertThat(asistenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAsistencia() throws Exception {
        // Initialize the database
        asistenciaRepository.saveAndFlush(asistencia);

        int databaseSizeBeforeDelete = asistenciaRepository.findAll().size();

        // Delete the asistencia
        restAsistenciaMockMvc.perform(delete("/api/asistencias/{id}", asistencia.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Asistencia> asistenciaList = asistenciaRepository.findAll();
        assertThat(asistenciaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

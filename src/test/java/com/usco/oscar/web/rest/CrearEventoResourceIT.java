package com.usco.oscar.web.rest;

import com.usco.oscar.Pp2OscarApp;
import com.usco.oscar.domain.CrearEvento;
import com.usco.oscar.repository.CrearEventoRepository;
import com.usco.oscar.service.CrearEventoService;
import com.usco.oscar.service.dto.CrearEventoDTO;
import com.usco.oscar.service.mapper.CrearEventoMapper;
import com.usco.oscar.service.dto.CrearEventoCriteria;
import com.usco.oscar.service.CrearEventoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CrearEventoResource} REST controller.
 */
@SpringBootTest(classes = Pp2OscarApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CrearEventoResourceIT {

    private static final String DEFAULT_NOMBRE_EVENTO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_EVENTO = "BBBBBBBBBB";

    private static final String DEFAULT_INFORMACION = "AAAAAAAAAA";
    private static final String UPDATED_INFORMACION = "BBBBBBBBBB";

    private static final String DEFAULT_LUGAR = "AAAAAAAAAA";
    private static final String UPDATED_LUGAR = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_HORA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_HORA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_USUARIO_ID = 1L;
    private static final Long UPDATED_USUARIO_ID = 2L;
    private static final Long SMALLER_USUARIO_ID = 1L - 1L;

    private static final Boolean DEFAULT_TIPO_EVENTO = false;
    private static final Boolean UPDATED_TIPO_EVENTO = true;

    private static final Long DEFAULT_CANTIDAD_BOLETAS = 1L;
    private static final Long UPDATED_CANTIDAD_BOLETAS = 2L;
    private static final Long SMALLER_CANTIDAD_BOLETAS = 1L - 1L;

    private static final String DEFAULT_CATEGORIA_EVENTO = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORIA_EVENTO = "BBBBBBBBBB";

    private static final Float DEFAULT_PRECIO_BOLETA = 1F;
    private static final Float UPDATED_PRECIO_BOLETA = 2F;
    private static final Float SMALLER_PRECIO_BOLETA = 1F - 1F;

    @Autowired
    private CrearEventoRepository crearEventoRepository;

    @Autowired
    private CrearEventoMapper crearEventoMapper;

    @Autowired
    private CrearEventoService crearEventoService;

    @Autowired
    private CrearEventoQueryService crearEventoQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCrearEventoMockMvc;

    private CrearEvento crearEvento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CrearEvento createEntity(EntityManager em) {
        CrearEvento crearEvento = new CrearEvento()
            .nombreEvento(DEFAULT_NOMBRE_EVENTO)
            .informacion(DEFAULT_INFORMACION)
            .lugar(DEFAULT_LUGAR)
            .fechaHora(DEFAULT_FECHA_HORA)
            .usuarioId(DEFAULT_USUARIO_ID)
            .tipoEvento(DEFAULT_TIPO_EVENTO)
            .cantidadBoletas(DEFAULT_CANTIDAD_BOLETAS)
            .categoriaEvento(DEFAULT_CATEGORIA_EVENTO)
            .precioBoleta(DEFAULT_PRECIO_BOLETA);
        return crearEvento;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CrearEvento createUpdatedEntity(EntityManager em) {
        CrearEvento crearEvento = new CrearEvento()
            .nombreEvento(UPDATED_NOMBRE_EVENTO)
            .informacion(UPDATED_INFORMACION)
            .lugar(UPDATED_LUGAR)
            .fechaHora(UPDATED_FECHA_HORA)
            .usuarioId(UPDATED_USUARIO_ID)
            .tipoEvento(UPDATED_TIPO_EVENTO)
            .cantidadBoletas(UPDATED_CANTIDAD_BOLETAS)
            .categoriaEvento(UPDATED_CATEGORIA_EVENTO)
            .precioBoleta(UPDATED_PRECIO_BOLETA);
        return crearEvento;
    }

    @BeforeEach
    public void initTest() {
        crearEvento = createEntity(em);
    }

    @Test
    @Transactional
    public void createCrearEvento() throws Exception {
        int databaseSizeBeforeCreate = crearEventoRepository.findAll().size();
        // Create the CrearEvento
        CrearEventoDTO crearEventoDTO = crearEventoMapper.toDto(crearEvento);
        restCrearEventoMockMvc.perform(post("/api/crear-eventos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crearEventoDTO)))
            .andExpect(status().isCreated());

        // Validate the CrearEvento in the database
        List<CrearEvento> crearEventoList = crearEventoRepository.findAll();
        assertThat(crearEventoList).hasSize(databaseSizeBeforeCreate + 1);
        CrearEvento testCrearEvento = crearEventoList.get(crearEventoList.size() - 1);
        assertThat(testCrearEvento.getNombreEvento()).isEqualTo(DEFAULT_NOMBRE_EVENTO);
        assertThat(testCrearEvento.getInformacion()).isEqualTo(DEFAULT_INFORMACION);
        assertThat(testCrearEvento.getLugar()).isEqualTo(DEFAULT_LUGAR);
        assertThat(testCrearEvento.getFechaHora()).isEqualTo(DEFAULT_FECHA_HORA);
        assertThat(testCrearEvento.getUsuarioId()).isEqualTo(DEFAULT_USUARIO_ID);
        assertThat(testCrearEvento.isTipoEvento()).isEqualTo(DEFAULT_TIPO_EVENTO);
        assertThat(testCrearEvento.getCantidadBoletas()).isEqualTo(DEFAULT_CANTIDAD_BOLETAS);
        assertThat(testCrearEvento.getCategoriaEvento()).isEqualTo(DEFAULT_CATEGORIA_EVENTO);
        assertThat(testCrearEvento.getPrecioBoleta()).isEqualTo(DEFAULT_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void createCrearEventoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = crearEventoRepository.findAll().size();

        // Create the CrearEvento with an existing ID
        crearEvento.setId(1L);
        CrearEventoDTO crearEventoDTO = crearEventoMapper.toDto(crearEvento);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCrearEventoMockMvc.perform(post("/api/crear-eventos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crearEventoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CrearEvento in the database
        List<CrearEvento> crearEventoList = crearEventoRepository.findAll();
        assertThat(crearEventoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCrearEventos() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList
        restCrearEventoMockMvc.perform(get("/api/crear-eventos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(crearEvento.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreEvento").value(hasItem(DEFAULT_NOMBRE_EVENTO)))
            .andExpect(jsonPath("$.[*].informacion").value(hasItem(DEFAULT_INFORMACION)))
            .andExpect(jsonPath("$.[*].lugar").value(hasItem(DEFAULT_LUGAR)))
            .andExpect(jsonPath("$.[*].fechaHora").value(hasItem(DEFAULT_FECHA_HORA.toString())))
            .andExpect(jsonPath("$.[*].usuarioId").value(hasItem(DEFAULT_USUARIO_ID.intValue())))
            .andExpect(jsonPath("$.[*].tipoEvento").value(hasItem(DEFAULT_TIPO_EVENTO.booleanValue())))
            .andExpect(jsonPath("$.[*].cantidadBoletas").value(hasItem(DEFAULT_CANTIDAD_BOLETAS.intValue())))
            .andExpect(jsonPath("$.[*].categoriaEvento").value(hasItem(DEFAULT_CATEGORIA_EVENTO)))
            .andExpect(jsonPath("$.[*].precioBoleta").value(hasItem(DEFAULT_PRECIO_BOLETA.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getCrearEvento() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get the crearEvento
        restCrearEventoMockMvc.perform(get("/api/crear-eventos/{id}", crearEvento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(crearEvento.getId().intValue()))
            .andExpect(jsonPath("$.nombreEvento").value(DEFAULT_NOMBRE_EVENTO))
            .andExpect(jsonPath("$.informacion").value(DEFAULT_INFORMACION))
            .andExpect(jsonPath("$.lugar").value(DEFAULT_LUGAR))
            .andExpect(jsonPath("$.fechaHora").value(DEFAULT_FECHA_HORA.toString()))
            .andExpect(jsonPath("$.usuarioId").value(DEFAULT_USUARIO_ID.intValue()))
            .andExpect(jsonPath("$.tipoEvento").value(DEFAULT_TIPO_EVENTO.booleanValue()))
            .andExpect(jsonPath("$.cantidadBoletas").value(DEFAULT_CANTIDAD_BOLETAS.intValue()))
            .andExpect(jsonPath("$.categoriaEvento").value(DEFAULT_CATEGORIA_EVENTO))
            .andExpect(jsonPath("$.precioBoleta").value(DEFAULT_PRECIO_BOLETA.doubleValue()));
    }


    @Test
    @Transactional
    public void getCrearEventosByIdFiltering() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        Long id = crearEvento.getId();

        defaultCrearEventoShouldBeFound("id.equals=" + id);
        defaultCrearEventoShouldNotBeFound("id.notEquals=" + id);

        defaultCrearEventoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCrearEventoShouldNotBeFound("id.greaterThan=" + id);

        defaultCrearEventoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCrearEventoShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByNombreEventoIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where nombreEvento equals to DEFAULT_NOMBRE_EVENTO
        defaultCrearEventoShouldBeFound("nombreEvento.equals=" + DEFAULT_NOMBRE_EVENTO);

        // Get all the crearEventoList where nombreEvento equals to UPDATED_NOMBRE_EVENTO
        defaultCrearEventoShouldNotBeFound("nombreEvento.equals=" + UPDATED_NOMBRE_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByNombreEventoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where nombreEvento not equals to DEFAULT_NOMBRE_EVENTO
        defaultCrearEventoShouldNotBeFound("nombreEvento.notEquals=" + DEFAULT_NOMBRE_EVENTO);

        // Get all the crearEventoList where nombreEvento not equals to UPDATED_NOMBRE_EVENTO
        defaultCrearEventoShouldBeFound("nombreEvento.notEquals=" + UPDATED_NOMBRE_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByNombreEventoIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where nombreEvento in DEFAULT_NOMBRE_EVENTO or UPDATED_NOMBRE_EVENTO
        defaultCrearEventoShouldBeFound("nombreEvento.in=" + DEFAULT_NOMBRE_EVENTO + "," + UPDATED_NOMBRE_EVENTO);

        // Get all the crearEventoList where nombreEvento equals to UPDATED_NOMBRE_EVENTO
        defaultCrearEventoShouldNotBeFound("nombreEvento.in=" + UPDATED_NOMBRE_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByNombreEventoIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where nombreEvento is not null
        defaultCrearEventoShouldBeFound("nombreEvento.specified=true");

        // Get all the crearEventoList where nombreEvento is null
        defaultCrearEventoShouldNotBeFound("nombreEvento.specified=false");
    }
                @Test
    @Transactional
    public void getAllCrearEventosByNombreEventoContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where nombreEvento contains DEFAULT_NOMBRE_EVENTO
        defaultCrearEventoShouldBeFound("nombreEvento.contains=" + DEFAULT_NOMBRE_EVENTO);

        // Get all the crearEventoList where nombreEvento contains UPDATED_NOMBRE_EVENTO
        defaultCrearEventoShouldNotBeFound("nombreEvento.contains=" + UPDATED_NOMBRE_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByNombreEventoNotContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where nombreEvento does not contain DEFAULT_NOMBRE_EVENTO
        defaultCrearEventoShouldNotBeFound("nombreEvento.doesNotContain=" + DEFAULT_NOMBRE_EVENTO);

        // Get all the crearEventoList where nombreEvento does not contain UPDATED_NOMBRE_EVENTO
        defaultCrearEventoShouldBeFound("nombreEvento.doesNotContain=" + UPDATED_NOMBRE_EVENTO);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByInformacionIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where informacion equals to DEFAULT_INFORMACION
        defaultCrearEventoShouldBeFound("informacion.equals=" + DEFAULT_INFORMACION);

        // Get all the crearEventoList where informacion equals to UPDATED_INFORMACION
        defaultCrearEventoShouldNotBeFound("informacion.equals=" + UPDATED_INFORMACION);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByInformacionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where informacion not equals to DEFAULT_INFORMACION
        defaultCrearEventoShouldNotBeFound("informacion.notEquals=" + DEFAULT_INFORMACION);

        // Get all the crearEventoList where informacion not equals to UPDATED_INFORMACION
        defaultCrearEventoShouldBeFound("informacion.notEquals=" + UPDATED_INFORMACION);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByInformacionIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where informacion in DEFAULT_INFORMACION or UPDATED_INFORMACION
        defaultCrearEventoShouldBeFound("informacion.in=" + DEFAULT_INFORMACION + "," + UPDATED_INFORMACION);

        // Get all the crearEventoList where informacion equals to UPDATED_INFORMACION
        defaultCrearEventoShouldNotBeFound("informacion.in=" + UPDATED_INFORMACION);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByInformacionIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where informacion is not null
        defaultCrearEventoShouldBeFound("informacion.specified=true");

        // Get all the crearEventoList where informacion is null
        defaultCrearEventoShouldNotBeFound("informacion.specified=false");
    }
                @Test
    @Transactional
    public void getAllCrearEventosByInformacionContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where informacion contains DEFAULT_INFORMACION
        defaultCrearEventoShouldBeFound("informacion.contains=" + DEFAULT_INFORMACION);

        // Get all the crearEventoList where informacion contains UPDATED_INFORMACION
        defaultCrearEventoShouldNotBeFound("informacion.contains=" + UPDATED_INFORMACION);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByInformacionNotContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where informacion does not contain DEFAULT_INFORMACION
        defaultCrearEventoShouldNotBeFound("informacion.doesNotContain=" + DEFAULT_INFORMACION);

        // Get all the crearEventoList where informacion does not contain UPDATED_INFORMACION
        defaultCrearEventoShouldBeFound("informacion.doesNotContain=" + UPDATED_INFORMACION);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByLugarIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where lugar equals to DEFAULT_LUGAR
        defaultCrearEventoShouldBeFound("lugar.equals=" + DEFAULT_LUGAR);

        // Get all the crearEventoList where lugar equals to UPDATED_LUGAR
        defaultCrearEventoShouldNotBeFound("lugar.equals=" + UPDATED_LUGAR);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByLugarIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where lugar not equals to DEFAULT_LUGAR
        defaultCrearEventoShouldNotBeFound("lugar.notEquals=" + DEFAULT_LUGAR);

        // Get all the crearEventoList where lugar not equals to UPDATED_LUGAR
        defaultCrearEventoShouldBeFound("lugar.notEquals=" + UPDATED_LUGAR);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByLugarIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where lugar in DEFAULT_LUGAR or UPDATED_LUGAR
        defaultCrearEventoShouldBeFound("lugar.in=" + DEFAULT_LUGAR + "," + UPDATED_LUGAR);

        // Get all the crearEventoList where lugar equals to UPDATED_LUGAR
        defaultCrearEventoShouldNotBeFound("lugar.in=" + UPDATED_LUGAR);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByLugarIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where lugar is not null
        defaultCrearEventoShouldBeFound("lugar.specified=true");

        // Get all the crearEventoList where lugar is null
        defaultCrearEventoShouldNotBeFound("lugar.specified=false");
    }
                @Test
    @Transactional
    public void getAllCrearEventosByLugarContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where lugar contains DEFAULT_LUGAR
        defaultCrearEventoShouldBeFound("lugar.contains=" + DEFAULT_LUGAR);

        // Get all the crearEventoList where lugar contains UPDATED_LUGAR
        defaultCrearEventoShouldNotBeFound("lugar.contains=" + UPDATED_LUGAR);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByLugarNotContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where lugar does not contain DEFAULT_LUGAR
        defaultCrearEventoShouldNotBeFound("lugar.doesNotContain=" + DEFAULT_LUGAR);

        // Get all the crearEventoList where lugar does not contain UPDATED_LUGAR
        defaultCrearEventoShouldBeFound("lugar.doesNotContain=" + UPDATED_LUGAR);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByFechaHoraIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where fechaHora equals to DEFAULT_FECHA_HORA
        defaultCrearEventoShouldBeFound("fechaHora.equals=" + DEFAULT_FECHA_HORA);

        // Get all the crearEventoList where fechaHora equals to UPDATED_FECHA_HORA
        defaultCrearEventoShouldNotBeFound("fechaHora.equals=" + UPDATED_FECHA_HORA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByFechaHoraIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where fechaHora not equals to DEFAULT_FECHA_HORA
        defaultCrearEventoShouldNotBeFound("fechaHora.notEquals=" + DEFAULT_FECHA_HORA);

        // Get all the crearEventoList where fechaHora not equals to UPDATED_FECHA_HORA
        defaultCrearEventoShouldBeFound("fechaHora.notEquals=" + UPDATED_FECHA_HORA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByFechaHoraIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where fechaHora in DEFAULT_FECHA_HORA or UPDATED_FECHA_HORA
        defaultCrearEventoShouldBeFound("fechaHora.in=" + DEFAULT_FECHA_HORA + "," + UPDATED_FECHA_HORA);

        // Get all the crearEventoList where fechaHora equals to UPDATED_FECHA_HORA
        defaultCrearEventoShouldNotBeFound("fechaHora.in=" + UPDATED_FECHA_HORA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByFechaHoraIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where fechaHora is not null
        defaultCrearEventoShouldBeFound("fechaHora.specified=true");

        // Get all the crearEventoList where fechaHora is null
        defaultCrearEventoShouldNotBeFound("fechaHora.specified=false");
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId equals to DEFAULT_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.equals=" + DEFAULT_USUARIO_ID);

        // Get all the crearEventoList where usuarioId equals to UPDATED_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.equals=" + UPDATED_USUARIO_ID);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId not equals to DEFAULT_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.notEquals=" + DEFAULT_USUARIO_ID);

        // Get all the crearEventoList where usuarioId not equals to UPDATED_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.notEquals=" + UPDATED_USUARIO_ID);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId in DEFAULT_USUARIO_ID or UPDATED_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.in=" + DEFAULT_USUARIO_ID + "," + UPDATED_USUARIO_ID);

        // Get all the crearEventoList where usuarioId equals to UPDATED_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.in=" + UPDATED_USUARIO_ID);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId is not null
        defaultCrearEventoShouldBeFound("usuarioId.specified=true");

        // Get all the crearEventoList where usuarioId is null
        defaultCrearEventoShouldNotBeFound("usuarioId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId is greater than or equal to DEFAULT_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.greaterThanOrEqual=" + DEFAULT_USUARIO_ID);

        // Get all the crearEventoList where usuarioId is greater than or equal to UPDATED_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.greaterThanOrEqual=" + UPDATED_USUARIO_ID);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId is less than or equal to DEFAULT_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.lessThanOrEqual=" + DEFAULT_USUARIO_ID);

        // Get all the crearEventoList where usuarioId is less than or equal to SMALLER_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.lessThanOrEqual=" + SMALLER_USUARIO_ID);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsLessThanSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId is less than DEFAULT_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.lessThan=" + DEFAULT_USUARIO_ID);

        // Get all the crearEventoList where usuarioId is less than UPDATED_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.lessThan=" + UPDATED_USUARIO_ID);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByUsuarioIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where usuarioId is greater than DEFAULT_USUARIO_ID
        defaultCrearEventoShouldNotBeFound("usuarioId.greaterThan=" + DEFAULT_USUARIO_ID);

        // Get all the crearEventoList where usuarioId is greater than SMALLER_USUARIO_ID
        defaultCrearEventoShouldBeFound("usuarioId.greaterThan=" + SMALLER_USUARIO_ID);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByTipoEventoIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where tipoEvento equals to DEFAULT_TIPO_EVENTO
        defaultCrearEventoShouldBeFound("tipoEvento.equals=" + DEFAULT_TIPO_EVENTO);

        // Get all the crearEventoList where tipoEvento equals to UPDATED_TIPO_EVENTO
        defaultCrearEventoShouldNotBeFound("tipoEvento.equals=" + UPDATED_TIPO_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByTipoEventoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where tipoEvento not equals to DEFAULT_TIPO_EVENTO
        defaultCrearEventoShouldNotBeFound("tipoEvento.notEquals=" + DEFAULT_TIPO_EVENTO);

        // Get all the crearEventoList where tipoEvento not equals to UPDATED_TIPO_EVENTO
        defaultCrearEventoShouldBeFound("tipoEvento.notEquals=" + UPDATED_TIPO_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByTipoEventoIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where tipoEvento in DEFAULT_TIPO_EVENTO or UPDATED_TIPO_EVENTO
        defaultCrearEventoShouldBeFound("tipoEvento.in=" + DEFAULT_TIPO_EVENTO + "," + UPDATED_TIPO_EVENTO);

        // Get all the crearEventoList where tipoEvento equals to UPDATED_TIPO_EVENTO
        defaultCrearEventoShouldNotBeFound("tipoEvento.in=" + UPDATED_TIPO_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByTipoEventoIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where tipoEvento is not null
        defaultCrearEventoShouldBeFound("tipoEvento.specified=true");

        // Get all the crearEventoList where tipoEvento is null
        defaultCrearEventoShouldNotBeFound("tipoEvento.specified=false");
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas equals to DEFAULT_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.equals=" + DEFAULT_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas equals to UPDATED_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.equals=" + UPDATED_CANTIDAD_BOLETAS);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas not equals to DEFAULT_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.notEquals=" + DEFAULT_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas not equals to UPDATED_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.notEquals=" + UPDATED_CANTIDAD_BOLETAS);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas in DEFAULT_CANTIDAD_BOLETAS or UPDATED_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.in=" + DEFAULT_CANTIDAD_BOLETAS + "," + UPDATED_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas equals to UPDATED_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.in=" + UPDATED_CANTIDAD_BOLETAS);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas is not null
        defaultCrearEventoShouldBeFound("cantidadBoletas.specified=true");

        // Get all the crearEventoList where cantidadBoletas is null
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.specified=false");
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas is greater than or equal to DEFAULT_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.greaterThanOrEqual=" + DEFAULT_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas is greater than or equal to UPDATED_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.greaterThanOrEqual=" + UPDATED_CANTIDAD_BOLETAS);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas is less than or equal to DEFAULT_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.lessThanOrEqual=" + DEFAULT_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas is less than or equal to SMALLER_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.lessThanOrEqual=" + SMALLER_CANTIDAD_BOLETAS);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsLessThanSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas is less than DEFAULT_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.lessThan=" + DEFAULT_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas is less than UPDATED_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.lessThan=" + UPDATED_CANTIDAD_BOLETAS);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCantidadBoletasIsGreaterThanSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where cantidadBoletas is greater than DEFAULT_CANTIDAD_BOLETAS
        defaultCrearEventoShouldNotBeFound("cantidadBoletas.greaterThan=" + DEFAULT_CANTIDAD_BOLETAS);

        // Get all the crearEventoList where cantidadBoletas is greater than SMALLER_CANTIDAD_BOLETAS
        defaultCrearEventoShouldBeFound("cantidadBoletas.greaterThan=" + SMALLER_CANTIDAD_BOLETAS);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByCategoriaEventoIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where categoriaEvento equals to DEFAULT_CATEGORIA_EVENTO
        defaultCrearEventoShouldBeFound("categoriaEvento.equals=" + DEFAULT_CATEGORIA_EVENTO);

        // Get all the crearEventoList where categoriaEvento equals to UPDATED_CATEGORIA_EVENTO
        defaultCrearEventoShouldNotBeFound("categoriaEvento.equals=" + UPDATED_CATEGORIA_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCategoriaEventoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where categoriaEvento not equals to DEFAULT_CATEGORIA_EVENTO
        defaultCrearEventoShouldNotBeFound("categoriaEvento.notEquals=" + DEFAULT_CATEGORIA_EVENTO);

        // Get all the crearEventoList where categoriaEvento not equals to UPDATED_CATEGORIA_EVENTO
        defaultCrearEventoShouldBeFound("categoriaEvento.notEquals=" + UPDATED_CATEGORIA_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCategoriaEventoIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where categoriaEvento in DEFAULT_CATEGORIA_EVENTO or UPDATED_CATEGORIA_EVENTO
        defaultCrearEventoShouldBeFound("categoriaEvento.in=" + DEFAULT_CATEGORIA_EVENTO + "," + UPDATED_CATEGORIA_EVENTO);

        // Get all the crearEventoList where categoriaEvento equals to UPDATED_CATEGORIA_EVENTO
        defaultCrearEventoShouldNotBeFound("categoriaEvento.in=" + UPDATED_CATEGORIA_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCategoriaEventoIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where categoriaEvento is not null
        defaultCrearEventoShouldBeFound("categoriaEvento.specified=true");

        // Get all the crearEventoList where categoriaEvento is null
        defaultCrearEventoShouldNotBeFound("categoriaEvento.specified=false");
    }
                @Test
    @Transactional
    public void getAllCrearEventosByCategoriaEventoContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where categoriaEvento contains DEFAULT_CATEGORIA_EVENTO
        defaultCrearEventoShouldBeFound("categoriaEvento.contains=" + DEFAULT_CATEGORIA_EVENTO);

        // Get all the crearEventoList where categoriaEvento contains UPDATED_CATEGORIA_EVENTO
        defaultCrearEventoShouldNotBeFound("categoriaEvento.contains=" + UPDATED_CATEGORIA_EVENTO);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByCategoriaEventoNotContainsSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where categoriaEvento does not contain DEFAULT_CATEGORIA_EVENTO
        defaultCrearEventoShouldNotBeFound("categoriaEvento.doesNotContain=" + DEFAULT_CATEGORIA_EVENTO);

        // Get all the crearEventoList where categoriaEvento does not contain UPDATED_CATEGORIA_EVENTO
        defaultCrearEventoShouldBeFound("categoriaEvento.doesNotContain=" + UPDATED_CATEGORIA_EVENTO);
    }


    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta equals to DEFAULT_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.equals=" + DEFAULT_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta equals to UPDATED_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.equals=" + UPDATED_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta not equals to DEFAULT_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.notEquals=" + DEFAULT_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta not equals to UPDATED_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.notEquals=" + UPDATED_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsInShouldWork() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta in DEFAULT_PRECIO_BOLETA or UPDATED_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.in=" + DEFAULT_PRECIO_BOLETA + "," + UPDATED_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta equals to UPDATED_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.in=" + UPDATED_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsNullOrNotNull() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta is not null
        defaultCrearEventoShouldBeFound("precioBoleta.specified=true");

        // Get all the crearEventoList where precioBoleta is null
        defaultCrearEventoShouldNotBeFound("precioBoleta.specified=false");
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta is greater than or equal to DEFAULT_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.greaterThanOrEqual=" + DEFAULT_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta is greater than or equal to UPDATED_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.greaterThanOrEqual=" + UPDATED_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta is less than or equal to DEFAULT_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.lessThanOrEqual=" + DEFAULT_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta is less than or equal to SMALLER_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.lessThanOrEqual=" + SMALLER_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsLessThanSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta is less than DEFAULT_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.lessThan=" + DEFAULT_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta is less than UPDATED_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.lessThan=" + UPDATED_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void getAllCrearEventosByPrecioBoletaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        // Get all the crearEventoList where precioBoleta is greater than DEFAULT_PRECIO_BOLETA
        defaultCrearEventoShouldNotBeFound("precioBoleta.greaterThan=" + DEFAULT_PRECIO_BOLETA);

        // Get all the crearEventoList where precioBoleta is greater than SMALLER_PRECIO_BOLETA
        defaultCrearEventoShouldBeFound("precioBoleta.greaterThan=" + SMALLER_PRECIO_BOLETA);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCrearEventoShouldBeFound(String filter) throws Exception {
        restCrearEventoMockMvc.perform(get("/api/crear-eventos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(crearEvento.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreEvento").value(hasItem(DEFAULT_NOMBRE_EVENTO)))
            .andExpect(jsonPath("$.[*].informacion").value(hasItem(DEFAULT_INFORMACION)))
            .andExpect(jsonPath("$.[*].lugar").value(hasItem(DEFAULT_LUGAR)))
            .andExpect(jsonPath("$.[*].fechaHora").value(hasItem(DEFAULT_FECHA_HORA.toString())))
            .andExpect(jsonPath("$.[*].usuarioId").value(hasItem(DEFAULT_USUARIO_ID.intValue())))
            .andExpect(jsonPath("$.[*].tipoEvento").value(hasItem(DEFAULT_TIPO_EVENTO.booleanValue())))
            .andExpect(jsonPath("$.[*].cantidadBoletas").value(hasItem(DEFAULT_CANTIDAD_BOLETAS.intValue())))
            .andExpect(jsonPath("$.[*].categoriaEvento").value(hasItem(DEFAULT_CATEGORIA_EVENTO)))
            .andExpect(jsonPath("$.[*].precioBoleta").value(hasItem(DEFAULT_PRECIO_BOLETA.doubleValue())));

        // Check, that the count call also returns 1
        restCrearEventoMockMvc.perform(get("/api/crear-eventos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCrearEventoShouldNotBeFound(String filter) throws Exception {
        restCrearEventoMockMvc.perform(get("/api/crear-eventos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCrearEventoMockMvc.perform(get("/api/crear-eventos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCrearEvento() throws Exception {
        // Get the crearEvento
        restCrearEventoMockMvc.perform(get("/api/crear-eventos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCrearEvento() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        int databaseSizeBeforeUpdate = crearEventoRepository.findAll().size();

        // Update the crearEvento
        CrearEvento updatedCrearEvento = crearEventoRepository.findById(crearEvento.getId()).get();
        // Disconnect from session so that the updates on updatedCrearEvento are not directly saved in db
        em.detach(updatedCrearEvento);
        updatedCrearEvento
            .nombreEvento(UPDATED_NOMBRE_EVENTO)
            .informacion(UPDATED_INFORMACION)
            .lugar(UPDATED_LUGAR)
            .fechaHora(UPDATED_FECHA_HORA)
            .usuarioId(UPDATED_USUARIO_ID)
            .tipoEvento(UPDATED_TIPO_EVENTO)
            .cantidadBoletas(UPDATED_CANTIDAD_BOLETAS)
            .categoriaEvento(UPDATED_CATEGORIA_EVENTO)
            .precioBoleta(UPDATED_PRECIO_BOLETA);
        CrearEventoDTO crearEventoDTO = crearEventoMapper.toDto(updatedCrearEvento);

        restCrearEventoMockMvc.perform(put("/api/crear-eventos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crearEventoDTO)))
            .andExpect(status().isOk());

        // Validate the CrearEvento in the database
        List<CrearEvento> crearEventoList = crearEventoRepository.findAll();
        assertThat(crearEventoList).hasSize(databaseSizeBeforeUpdate);
        CrearEvento testCrearEvento = crearEventoList.get(crearEventoList.size() - 1);
        assertThat(testCrearEvento.getNombreEvento()).isEqualTo(UPDATED_NOMBRE_EVENTO);
        assertThat(testCrearEvento.getInformacion()).isEqualTo(UPDATED_INFORMACION);
        assertThat(testCrearEvento.getLugar()).isEqualTo(UPDATED_LUGAR);
        assertThat(testCrearEvento.getFechaHora()).isEqualTo(UPDATED_FECHA_HORA);
        assertThat(testCrearEvento.getUsuarioId()).isEqualTo(UPDATED_USUARIO_ID);
        assertThat(testCrearEvento.isTipoEvento()).isEqualTo(UPDATED_TIPO_EVENTO);
        assertThat(testCrearEvento.getCantidadBoletas()).isEqualTo(UPDATED_CANTIDAD_BOLETAS);
        assertThat(testCrearEvento.getCategoriaEvento()).isEqualTo(UPDATED_CATEGORIA_EVENTO);
        assertThat(testCrearEvento.getPrecioBoleta()).isEqualTo(UPDATED_PRECIO_BOLETA);
    }

    @Test
    @Transactional
    public void updateNonExistingCrearEvento() throws Exception {
        int databaseSizeBeforeUpdate = crearEventoRepository.findAll().size();

        // Create the CrearEvento
        CrearEventoDTO crearEventoDTO = crearEventoMapper.toDto(crearEvento);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCrearEventoMockMvc.perform(put("/api/crear-eventos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crearEventoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CrearEvento in the database
        List<CrearEvento> crearEventoList = crearEventoRepository.findAll();
        assertThat(crearEventoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCrearEvento() throws Exception {
        // Initialize the database
        crearEventoRepository.saveAndFlush(crearEvento);

        int databaseSizeBeforeDelete = crearEventoRepository.findAll().size();

        // Delete the crearEvento
        restCrearEventoMockMvc.perform(delete("/api/crear-eventos/{id}", crearEvento.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CrearEvento> crearEventoList = crearEventoRepository.findAll();
        assertThat(crearEventoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
